var express = require('express');
var router = express.Router();
const bcrypt = require("bcrypt")
const fs = require("fs");

router.get('/', function (req, res, next) {
    res.render('authorization_page',
        {
            title: 'Авторизация',
            link_css: '<link rel="stylesheet" href="/stylesheets/authorization_page.css">'
        });
});
router.get('/rating', function (req, res, next) {
    res.render('rating-page',
        {
            title: 'Рейтинг',
            link_css: '<link rel="stylesheet" href="/stylesheets/rating_page.css">',
        });
});
router.post('/', function (req, res, next) {
    let user = req.body.usernames
    let pass = req.body.password
    const data = fs.readFileSync('../esofteducation/hesh.txt', 'utf8');
    const lines = data.split('\n');
    let hesh1 = lines[0];
    /////////////////////////////////////////////////////
    //Логин и пароль - qwerty
    /////////////////////////////////////////////////////
    if (compare(user,hesh1) === true  && compare(pass,hesh1) === true ) {
        res.render('rating-page',
            {
                title: 'Рейтинг',
                link_css: '<link rel="stylesheet" href="/stylesheets/rating_page.css">',
            });
    } else {
        res.render('authorization_page',
            {
                title: 'Авторизация',
                link_css: '<link rel="stylesheet" href="/stylesheets/authorization_page.css">'
            });
    }
});
module.exports = router;

function compare(input, hash) {
    return bcrypt.compareSync(input, hash);
}

