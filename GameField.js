class GameField {
    //свойство для хранения информации активного игрока (крестики или нолики), первые ходят крестики
    mode = 'x';
    //Информация о клетках поля, изначально все клетки пустые
    state = [
        [null, null, null],
        [null, null, null],
        [null, null, null]
    ];
    //Проверяет наличие 3 ряда О или Х
    checkWin(symbol) {

        for (let i = 0; i < this.state.length; i++) {
            let j = 0
            // Проверка по горизонтали
            if (this.state[i][j] === symbol) {
                if (this.state[i][j + 1] === symbol) {
                    if (this.state[i][j + 2] === symbol) {
                        return true
                    }
                }
            }
            // Проверка по вертикали
            if (this.state[j][i] === symbol) {
                if (this.state[j + 1][i] === symbol) {
                    if (this.state[j + 2][i] === symbol) {
                        return true
                    }
                }
            }

        }
        // Проверка по диагонали
        if (this.state[0][0] === symbol) {
            if (this.state[1][1] === symbol) {
                if (this.state[2][2] === symbol) {
                    return true
                }
            }
        }
        if (this.state[0][2] === symbol) {
            if (this.state[1][1] === symbol) {
                if (this.state[2][0] === symbol) {
                    return true
                }
            }
        }


        return false

    }

    // Определяет есть ли победитель или игра продолжается
    isOverGame() {
        switch (true) {
            case this.checkWin('x') === true && this.checkWin('o') === false:
                console.log('Крестики победили');
                return true;
            case this.checkWin('o') === true && this.checkWin('x') === false:
                console.log('Нолики победили');
                return true;
            case this.checkWin('o') === true && this.checkWin('x') === true:
                console.log('Ничья');
                return true;
            case (this.checkWin('o') === false && this.checkWin('x')) && this.state.includes(null) === false:
                console.log('Ничья');
                return true;
        }
    }
    //Метод для изменения активного игрока
    setMode() {
        this.mode === 'o' ? this.mode = 'x' : this.mode = 'o';
    }
    //Метод для заполнения клетки поле
    FieldCellValue() {
        let logic = false;
        while (logic === false) {
            let line = prompt('Введите строку хода?');
            let col = prompt('Введите столбец хода?');
            if (this.state[line][col] === null) {
                this.state[line][col] = this.mode;
                logic = true;
            } else {
                console.log('В координату' + '[' + line + ']' + '[' + col + ']' + 'нельзя сделать ход');
            }
        }
    }
}
//Пример работы
let game = new GameField();

while (!game.isOverGame()) {
    game.FieldCellValue();
    game.setMode();
}
