let logic = false;
const cellPLay1 = document.getElementById("cell1");
const cellPLay2 = document.getElementById("cell2");
const cellPLay3 = document.getElementById("cell3");
const cellPLay4 = document.getElementById("cell4");
const cellPLay5 = document.getElementById("cell5");
const cellPLay6 = document.getElementById("cell6");
const cellPLay7 = document.getElementById("cell7");
const cellPLay8 = document.getElementById("cell8");
const cellPLay9 = document.getElementById("cell9");
const imgCross_Or_Zero = document.getElementById("zero_or_cross");
let photoZero = document.createElement("img");
let photoCross = document.createElement("img");
photoZero.src = "../static/images/zero.svg"
photoCross.src = "../static/images/cross.svg"

function createCross() {
    let img = document.createElement("img");
    img.setAttribute("src", "../static/images/big_cross.svg");
    return img;
}
function createZero() {
    let img = document.createElement("img");
    img.setAttribute("src", "../static/images/big_zero.svg");
    return img;
}
function change_player()
{
    if(logic===false){
        photoZero.remove();
        imgCross_Or_Zero.appendChild(photoCross);
        document.getElementById('now_play').textContent = 'Плюшкина Екатерина';

    }
    else{
        photoCross.remove();
        imgCross_Or_Zero.appendChild(photoZero);
        document.getElementById('now_play').textContent = 'Пупкин Владлен';
    }
}
cellPLay1.addEventListener('click', () => {
    if (cellPLay1.innerHTML.includes('<img')) {
        console.log('Есть изображение');
    } else {
        if (logic===false)
        {
            cellPLay1.appendChild(createCross());
            logic = true;
        }
        else{
            cellPLay1.appendChild(createZero());
            logic = false;
        }
    }
    change_player();
});
cellPLay2.addEventListener('click', () => {
    if (cellPLay2.innerHTML.includes('<img')) {
        console.log('Есть изображение');
    } else {
        if (logic===false)
        {
            cellPLay2.appendChild(createCross());
            logic = true;
        }
        else{
            cellPLay2.appendChild(createZero());
            logic = false;
        }
    }
    change_player();
});
cellPLay3.addEventListener('click', () => {
    if (cellPLay3.innerHTML.includes('<img')) {
        console.log('Есть изображение');
    } else {
        if (logic===false)
        {
            cellPLay3.appendChild(createCross());
            logic = true;
        }
        else{
            cellPLay3.appendChild(createZero());
            logic = false;
        }
    }
    change_player();
});
cellPLay4.addEventListener('click', () => {
    if (cellPLay4.innerHTML.includes('<img')) {
        console.log('Есть изображение');
    } else {
        if (logic===false)
        {
            cellPLay4.appendChild(createCross());
            logic = true;
        }
        else{
            cellPLay4.appendChild(createZero());
            logic = false;
        }
    }
    change_player();

});
cellPLay5.addEventListener('click', () => {
    if (cellPLay5.innerHTML.includes('<img')) {
        console.log('Есть изображение');
    } else {
        if (logic===false)
        {
            cellPLay5.appendChild(createCross());
            logic = true;
        }
        else{
            cellPLay5.appendChild(createZero());
            logic = false;
        }
    }
    change_player();

});
cellPLay6.addEventListener('click', () => {
    if (cellPLay6.innerHTML.includes('<img')) {
        console.log('Есть изображение');
    } else {
        if (logic===false)
        {
            cellPLay6.appendChild(createCross());
            logic = true;
        }
        else{
            cellPLay6.appendChild(createZero());
            logic = false;
        }
    }
    change_player();

});
cellPLay7.addEventListener('click', () => {
    if (cellPLay7.innerHTML.includes('<img')) {
        console.log('Есть изображение');
    } else {
        if (logic===false)
        {
            cellPLay7.appendChild(createCross());
            logic = true;
        }
        else{
            cellPLay7.appendChild(createZero());
            logic = false;
        }
    }
    change_player();

});
cellPLay8.addEventListener('click', () => {
    if (cellPLay8.innerHTML.includes('<img')) {
        console.log('Есть изображение');
    } else {
        if (logic===false)
        {
            cellPLay8.appendChild(createCross());
            logic = true;
        }
        else{
            cellPLay8.appendChild(createZero());
            logic = false;
        }
    }
    change_player();

});
cellPLay9.addEventListener('click', () => {
    if (cellPLay9.innerHTML.includes('<img')) {
        console.log('Есть изображение');
    } else {
        if (logic===false)
        {
            cellPLay9.appendChild(createCross());
            logic = true;
        }
        else{
            cellPLay9.appendChild(createZero());
            logic = false;
        }
    }
    change_player();

});
if(logic===false){
    photoZero.remove();
    imgCross_Or_Zero.appendChild(photoCross);
    document.getElementById('now_play').textContent = '  Плюшкина Екатерина';


}
else{
    photoCross.remove();
    imgCross_Or_Zero.appendChild(photoZero);
    document.getElementById('now_play').textContent = '  Пупкин Владлен';
}
