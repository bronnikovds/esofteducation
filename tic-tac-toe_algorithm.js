const gameField = [
    ['x', 'o', null],
    ['x', null, 'o'],
    ['x', 'o', 'o']
];
winner = false

function checkkWin(symbol) {

    for (let i = 0; i < gameField.length; i++) {
        let j = 0
        // Проверка по горизонтали
        if (gameField[i][j] === symbol) {
            if (gameField[i][j + 1] === symbol) {
                if (gameField[i][j + 2] === symbol) {
                    return true
                }
            }
        }
        // Проверка по вертикали
        if (gameField[j][i] === symbol) {
            if (gameField[j + 1][i] === symbol) {
                if (gameField[j + 2][i] === symbol) {
                    return true
                }
            }
        }

    }
    // Проверка по диагонали
    if (gameField[0][0] === symbol) {
        if (gameField[1][1] === symbol) {
            if (gameField[2][2] === symbol) {
                return true
            }
        }
    }
    if (gameField[0][2] === symbol) {
        if (gameField[1][1] === symbol) {
            if (gameField[2][0] === symbol) {
                return true
            }
        }
    }


    return false

}

switch (true) {
    case checkWin('x') === true && checkWin('o') === false:
        console.log('Крестики победили');
        break;
    case checkWin('o') === true && checkWin('x') === false:
        console.log('Нолики победили');
        break;
    case checkWin('o') === true && checkWin('x') === true:
        console.log('Ничья');
        break;
    case checkWin('o') === false && checkWin('x') === false:
        console.log('Ничья');
        break;
}

